## pkg-config-test for DUNE

This project contains a colelction of tests to make sure that the DUNE
core modules are usable without `cmake`, simply using `pkg-config`.

To test it yourself do the following steps:

### clone (some) core repositories
```
./prepare.sh getsrc
```

This will create a subdirectory `source` which contains the different
modules. By default `getsrc` with try to clone the modules with the
same branch as used in `pkg-config-test`.

You now have to opportunity to change the branch of one of the modules manually.

### build and install DUNE

```
./prepare.sh build
```
configures and builds the dune modules. The configuration is read from
the `opts` file in the main directory of this project. We build all
modules in the `build` folder.

```
./prepare.sh install
```

installs all modules into the folder `install`.

### run tests

For different modules we have different test directories, where we try
to build tests (mostly copied from the core modules) without
`cmake`. The directories contain plain `Makefile`s and `make.inc`
contains the calls to `pkg-config` to extract the different options.

