PREFIX ?= ../install
LIBDIRNAME ?= /lib
DUNE_PKG_DIR = $(PREFIX)$(LIBDIRNAME)/pkgconfig/
DUNE_AUX_PKG_DIR = $(DUNE_PKG_DIR)/dune
DUNE_CXXFLAGS = $(shell export PKG_CONFIG_PATH=$(PKG_CONFIG_PATH):$(DUNE_PKG_DIR):$(DUNE_AUX_PKG_DIR); pkg-config --cflags $(DUNE_DEPS))
DUNE_LDFLAGS = $(shell export PKG_CONFIG_PATH=$(PKG_CONFIG_PATH):$(DUNE_PKG_DIR):$(DUNE_AUX_PKG_DIR); pkg-config --libs-only-L $(DUNE_DEPS))
DUNE_LDLIBS = $(shell export PKG_CONFIG_PATH=$(PKG_CONFIG_PATH):$(DUNE_PKG_DIR):$(DUNE_AUX_PKG_DIR); pkg-config --libs-only-l --libs-only-other $(DUNE_DEPS))
CXXFLAGS = -g -Wall -I.. $(DUNE_CXXFLAGS)
LDFLAGS = $(DUNE_LDFLAGS)
LDLIBS = $(DUNE_LDLIBS)

CC=g++
CXX=g++

all: $(TARGETS)

clean:
	rm -f *~
	rm -f *.o
	rm -f $(TARGETS)

info:
	echo $(DUNE_CXXFLAGS)
	echo $(DUNE_LDFLAGS)
	echo $(DUNE_LIBS)
