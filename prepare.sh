#!/bin/bash

# bail on errors
set -e
set -o pipefail # also detect error, while piping output through tee

export BASEDIR="$(
  cd "$(dirname "$0")" >/dev/null 2>&1
  pwd -P
)"
export SOURCEDIR=$BASEDIR/source
export INSTALLDIR=$BASEDIR/install
export BUILDDIR=$BASEDIR/build
export DUNE_BUILDDIR="$BUILDDIR"

# basic variable
# branch_name="$(git symbolic-ref HEAD 2>/dev/null | sed -e 's,.*/\(.*\),\1,')" || branch_name=master
# gitbaseurl=https://gitlab.dune-project.org/core
gitbaseurl=ssh://git@gitlab.dune-project.org:22022/core

# helper functions
clone() 
{
  mkdir -p $SOURCEDIR
  cd $SOURCEDIR
  # clone
  name=$1
  branch_name=$2
  git clone --branch $branch_name $gitbaseurl/$name
}

getsrc()
{
  clone dune-common fix-pkg-config
  clone dune-istl fix-pkg-config
  clone dune-geometry fix-pkg-config
  clone dune-grid master
}

build()
{
  mkdir -p $BUILDDIR
  mkdir -p $INSTALLDIR
  $SOURCEDIR/dune-common/bin/dunecontrol --opts=$BASEDIR/opts cmake
  $SOURCEDIR/dune-common/bin/dunecontrol --opts=$BASEDIR/opts make -j 2
}

install()
{
  $SOURCEDIR/dune-common/bin/dunecontrol --opts=$BASEDIR/opts make install
  # cp $BUILDDIR/dune-common/config.h $BASEDIR
}

runtest()
{
  true
}

clean() 
{
  rm -fr $SOURCEDIR
  rm -fr $BUILDDIR
  rm -fr $INSTALLDIR
}

#main script starts here
reporterror() {
  echo "Terminating $(basename "$0") due to previous errors!" >&2
  echo "See log file ${BUILD_DIR}/build_log.txt for details." >&2
  exit 1
}

trap reporterror EXIT
echo $BASEDIR
case $1 in
	getsrc|build|install|runtest|clean)
		echo "=== run command >>$1<<"
		$1
		;;
	*)
	    echo "=== unknown command >>$1<<"
		exit -1
		;;
esac
trap - EXIT

exit 0
